# XZYComm

#### 介绍
串口通讯控件(delphi)

#### 软件架构
串口通讯组件修改自 小猪工作室 Small-Pig Team （中国台湾）的SPCOMM 串口通讯组件

最新版本 V3.3 支持delphi11，Delphi12


#### 安装教程

1.  配置XZYComm.inc ,适应delphi版本
2.  根据编译版本，修改!AdminBuild_XZYComm.cmd 文件中 108 行 "set VCLVersion=29"
3.  运行 !AdminBuild_XZYComm.cmd 进行编译
4.  启动delphi，挂接编译好的bpl文件,添加相应的路径到delphi的库路径中。

### 版本
- Version 2.51   2002/3/15
    > 基于Spcomm 2.5改写。
- Version 2.6    2008/3/5
    > Add Eof char,Evt char;
- Version 2.01   2015/5/13
    > 修正不能打开Com10以上Bug
- Version 2.02   2018/6/16
    > 修正错误提示信息
- Version 3.0    2020/6/12
    > 兼容Delphi10.3
    > 升级到delphi 10.3.3
    > 修复 Parity 设置Bug , szInputBuffer 修改为@szInputBuffer;
- Version 3.01		2020/6/16
    > Modify some error from source code,and can send data without
    > lose any byte.Modified some error about the SENDEMPTY property,
    > so it can be checked in applicaiton.
- Version 3.1		2020/6/17
    > Add new property Connected;
    > 兼容Delphi XE以上版本，Char改为AnsiChar
- Version 3.3		2021/11/17
    > 兼容Delphi 11 ，Delphi12
